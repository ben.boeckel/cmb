# We require cmb's version information to be included the CMake Project
# declaration in order to properly include cmb versions when using cmb as an
# SDK. We therefore do not have access to the variable ${PROJECT_SOURCE_DIR}
# at the time cmbVersion.cmake is read. We therefore use
# ${CMAKE_CURRENT_SOURCE_DIR}, since CMBVersion.cmake is called at the top
# level of cmb's build. We also guard against subsequent calls to
# CMBVersion.cmake elsewhere in the build setup where
# ${CMAKE_CURRENT_SOURCE_DIR} may no longer be set to the top level directory.

if (NOT DEFINED cmb_version)

  file(STRINGS ${CMAKE_CURRENT_SOURCE_DIR}/version.txt version_string )

  string(REGEX MATCH "([0-9]+)\\.([0-9]+)\\.([0-9]+)[-]*(.*)"
    version_matches "${version_string}")

  set(cmb_version_major ${CMAKE_MATCH_1})
  set(cmb_version_minor ${CMAKE_MATCH_2})
  set(cmb_version_patch "${CMAKE_MATCH_3}")
  # Do we just have a patch version or are there extra stuff?
  if (DEFINED CMAKE_MATCH_4)
    set(cmb_version_patch "${CMAKE_MATCH_3}-${CMAKE_MATCH_4}")
  endif()
  set(cmb_version "${cmb_version_major}.${cmb_version_minor}")

endif()
