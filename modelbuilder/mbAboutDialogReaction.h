//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#ifndef mbAboutDialogReaction_h
#define mbAboutDialogReaction_h

#include "pqReaction.h"

/**
* @ingroup Reactions
* mbAboutDialogReaction used to show the standard about dialog for the
* application.
*/
class mbAboutDialogReaction : public pqReaction
{
  Q_OBJECT
  using Superclass = pqReaction;

public:
  mbAboutDialogReaction(QAction* parent);

  /**
  * Shows the about dialog for the application.
  */
  static void showAboutDialog();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { mbAboutDialogReaction::showAboutDialog(); }

private:
  Q_DISABLE_COPY(mbAboutDialogReaction)
};

#endif
