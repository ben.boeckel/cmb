=====
Usage
=====

CMB may be configured to use custom branding, such as icons, splash screens, and the about dialog, by setting properties for the project.

By default, the ``modelbuilder`` application will be built, and the CMakeLists.txt and resources to do so can be found under the ``modelbuilder/`` directory.

==========
Properties
==========

To set properties for CMB, use the ``cmb_set_property`` API.

.. code:: cmake

   cmb_set_property(NAME "MyApplicationName")

Properties can be broken into three groups, Application, Resource, and Information.

.. csv-table:: Application Properties
   :file: ./tables/application-properties-table.csv
   :widths: 15, 30, 55
   :header-rows: 1

Application properties are used to configure how different system will display the application in menus:

.. csv-table:: Resourece Properties
   :file: ./tables/resource-properties-table.csv
   :widths: 15, 30, 55
   :header-rows: 1

Resource properties are used to direct the application to image and file resources:

Using the property ``RESOURCE_DIR`` acts as a replacement for the default search path for resources that are not explicitly specified. The heirachy for searching for resources is hardcoded and if a resource is not found it will result in an error.


.. csv-table:: Infomation Properties
   :file: ./tables/information-properties-table.csv
   :widths: 15, 30, 55
   :header-rows: 1

Information Properties will show up under the help menu to populate links and/or by the ParaView client when building the application.

============
About Dialog
============

Overriding the help dialog requires a QObject class to be defined. A reasonable template to start from can be found in ``modelbuilder/mbAboutDialogReaction{.h,.cxx}``. To configure the About dialog use the ``cmb_configure_about_dialog`` CMake API provided by CMB.

example:

.. code:: cmake

 cmb_configure_about_dialog(
   CLASS_NAME mbAboutDialogReaction
   SOURCES
     mbAboutDialogReaction.cxx
     mbAboutDialogReaction.h)

============================
Default Layout Specification
============================

Configure the default layout of the Qt dock widgets using the ``cmb_configure_layout_spec`` CMake API. A reasonable template to start from can be found in ``modelbuilder/mbLayoutSpec{.h,.cxx}``.

example:

.. code:: cmake

   cmb_configure_layout_spec(
     CLASS_NAME mbLayoutSpec
     SOURCES
       mbLayoutSpec.cxx
       mbLayoutSpec.h)
