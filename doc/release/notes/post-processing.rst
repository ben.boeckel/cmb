Post-processing control moved to SMTK
-------------------------------------

In order for SMTK's attribute panel to modify its behavior depending on
whether CMB is in post-processing mode or not, the state variable
that controls post-processing mode has been moved into SMTK's pqSMTKBehavior
class.

The toolbar button and UI code to effect post-processing mode remain in
this repository (CMB). However, now it is possible for other SMTK
elements to initiate post-processing mode.
